# Black hole sensitivities in Einstein-scalar-Gauss-Bonnet gravity #

This repository contains the supplementary material to the paper,

> "Black hole sensitivities in Einstein-scalar-Gauss-Bonnet gravity",
> Félix-Louis Julié, Hector O. Silva, Emanuele Berti and Nicolás Yunes,
> [Phys. Rev. D 105 124031 (2022)](https://journals.aps.org/prd/abstract/10.1103/PhysRevD.105.124031), 
> [(2202.01329)](https://arxiv.org/abs/2202.01329)

If you use any of the material found here, please cite the paper as

```
@article{Julie:2022huo,
    author = "Juli\'e, F\'elix-Louis and Silva, Hector O. and Berti, Emanuele and Yunes, Nicol\'as",
    title = "{Black hole sensitivities in Einstein-scalar-Gauss-Bonnet gravity}",
    eprint = "2202.01329",
    archivePrefix = "arXiv",
    primaryClass = "gr-qc",
    doi = "10.1103/PhysRevD.105.124031",
    journal = "Phys. Rev. D",
    volume = "105",
    number = "12",
    pages = "124031",
    year = "2022"
}
```
